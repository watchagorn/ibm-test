testpath = "./testcases"
retryprefix = "--prerunmodifier DataDriver.rerunfailed:output.xml --output rerun.xml -r rerunreport.html"
max_retry = 1

def web_run_test_with_retry(testcaseDir,includeTag,excludeTag,thread,gosoftVersion){
    def CONTAINER_TEST_DIR = "/robot/testcases"
    CONTAINER_TEST_DIR = "/robot/testcases" + testcaseDir
    echo "CONTAINER_TEST_DIR: ${CONTAINER_TEST_DIR}"
    sh "docker --version"
    try {
        withCredentials([usernamePassword( credentialsId: 'dockerhub', usernameVariable: 'USER', passwordVariable: 'PASSWORD')]) {
            sh """
            docker login -u $USER -p $PASSWORD
            docker pull gosoftdockers/gosoft-rbf-with-browser:${gosoftVersion}
            docker tag gosoftdockers/gosoft-rbf-with-browser:${gosoftVersion} gosoftdockers/gosoft-rbf-with-browser:ibm-driveout
            docker run -m 4G --cpus='2.0' --name robotframework_docker_$BUILD_NUMBER \
            -v /dev/shm:/dev/shm \
            -v $WORKSPACE/reports:/robot/reports \
            -v $WORKSPACE:/robot/testcases \
            -e PABOT_OPTIONS='--pabotlib' \
            -e ROBOT_THREADS=${thread} \
            -e ROBOT_TESTS_DIR='${CONTAINER_TEST_DIR}' \
            -e ROBOT_OPTIONS='${includeTag} ${excludeTag} --outputdir /robot/reports' \
            gosoftdockers/gosoft-rbf-with-browser:ibm-driveout
            """
        }//end withCredentials
        echo "Login success"
    }// end try

    catch (err) {
        def max_retry = 1
        for(int i =0; i<max_retry; i++){
            sh "docker stop robotframework_docker_$BUILD_NUMBER"
            sh "docker rm -f robotframework_docker_$BUILD_NUMBER"
            echo "removed container robotframework_docker_$BUILD_NUMBER"
            try{
                echo "Running retry logic ${i}"
                sh "ls -la"
                sh "docker run -m 4G --cpus='2.0' --name robotframework_docker_$BUILD_NUMBER \
                -v /dev/shm:/dev/shm \
                -v $WORKSPACE/reports:/robot/reports \
                -v $WORKSPACE:/robot/testcases \
                -e PABOT_OPTIONS='--pabotlib --prerunmodifier DataDriver.rerunfailed:/robot/reports/output.xml --output rerun.xml -r rerunreport.html' \
                -e ROBOT_THREADS=${thread} \
                -e ROBOT_TESTS_DIR='${CONTAINER_TEST_DIR}' \
                -e ROBOT_OPTIONS='--outputdir /robot/reports' \
                gosoftdockers/gosoft-rbf-with-browser:ibm-driveout"
            }
            catch (err2){
                echo "Nothing need to implement"
            }

            try{
                sh "ls -la"
                sh 'rebot -r reports/final_report -o reports/final_report -l reports/log --merge reports/output.xml  reports/rerun.xml'
            }
            catch (err3){
                echo "Nothing need to implement"
            }

            try {
                sh """
                ls -la
                echo 'Merged result done'
                mv reports/report.html reports/${i}_report.html
                mv reports/output.xml reports/${i}_output.xml
                mv reports/final_report.xml reports/output.xml
                mv reports/final_report.html reports/report.html
                echo 'Rename result done'
                ls -la
                """
            }
            catch (err4){
                echo "nothing to rename"
            }
        }//end for

    }// end catch    

}// end web_run_test_with_retry

def dryrun_with_robocop(testcaseDir, thread, gosoftVersion){
    def CONTAINER_TEST_DIR = "/robot/testcases"
    CONTAINER_TEST_DIR = "/robot/testcases" + testcaseDir
    echo "CONTAINER_TEST_DIR: ${CONTAINER_TEST_DIR}"
    sh "docker --version"
    try {
        withCredentials([usernamePassword( credentialsId: 'dockerhub', usernameVariable: 'USER', passwordVariable: 'PASSWORD')]) {
            sh "docker login -u $USER -p $PASSWORD"
            sh "docker pull gosoftdockers/gosoft-rbf-with-browser:${gosoftVersion}"
            sh "docker tag gosoftdockers/gosoft-rbf-with-browser:${gosoftVersion} gosoftdockers/gosoft-rbf-with-browser:ibm-driveout"
            sh """docker run -m 4G --cpus='2.0' --name robotframework_docker_$BUILD_NUMBER \
            -v /dev/shm:/dev/shm \
            -v $WORKSPACE/reports:/robot/reports \
            -v $WORKSPACE:/robot/testcases \
            -e PABOT_OPTIONS='--pabotlib' \
            -e ROBOT_THREADS=${thread} \
            -e ROBOT_TESTS_DIR='${CONTAINER_TEST_DIR}' \
            -e ROBOT_OPTIONS='--dryrun -v env:jenkins --outputdir /robot/reports' \
            gosoftdockers/gosoft-rbf-with-browser:ibm-driveout \
            """
            sh """docker run -m 4G --cpus='2.0' --name robotframework_docker_robocop_$BUILD_NUMBER \
            -v /dev/shm:/dev/shm \
            -v $WORKSPACE/reports:/robot/reports \
            -v $WORKSPACE:/robot/testcases \
            gosoftdockers/gosoft-rbf-with-browser:ibm-driveout \
            robocop -A /robot/testcases/robocop.cfg
            """
        }//end withCredentials
        return true
    }// end try
    catch (err_robocop){
        echo "Found exception dryrun and robocop : ${err_robocop}"
        return false
    }
}



def notify_retuly_to_line(lineToken) {
    echo "sending result to Line"
    def failed_count = tm('${ROBOT_FAILED}').toInteger()
    def passed_count = tm('${ROBOT_PASSED}').toInteger()
    def robotlog = "${BUILD_URL}" + "/robot/report/log.html"
    robotlog = robotlog.replaceAll( 'localhost', '111.123.122.99' )
    sh """curl --location --request POST 'https://notify-api.line.me/api/notify' \
    --header 'Authorization: Bearer ${lineToken}' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data-urlencode 'message= ${JOB_NAME}
    BUILD_NUMBER : ${BUILD_NUMBER}
    BRANCH : ${GIT_BRANCH}
    PASSED : ${passed_count}
    FAILED : ${failed_count}
    EXECUTION TIME : ${currentBuild.durationString}
    REPORT : ${robotlog}'"""
}

def notify_result_to_msteam(webhookUrl) {
    echo "Publish Robot Framework test results to Microsoft Team"
    def failed_count = tm('${ROBOT_FAILED}').toInteger()
    def passed_count = tm('${ROBOT_PASSED}').toInteger()
    def robotlog = "${BUILD_URL}" + "/robot/report/log.html"
    robotlog = robotlog.replaceAll( 'localhost', '111.123.122.110' )
    echo "${robotlog}"
    office365ConnectorSend webhookUrl: "${webhookUrl}",
        factDefinitions: [
            [name: "BUILD_NUMBER ", template: "${BUILD_NUMBER}"],
            [name: "BRANCH :", template: "${GIT_BRANCH}"],
            [name: "PASSED : ", template: "${passed_count}"],
            [name: "FAILED : ", template: "${failed_count}"],
            [name: "EXECUTION TIME :", template: " ${currentBuild.durationString}"],
            [name: "REPORT : ", template: "[report](${robotlog})"]
        ]
}


def set_routing_table(access_database_sth) {
    sh """
    sudo route add -net ${access_database_sth}
    echo "Set routing table for access database sth"
    """
}


return this
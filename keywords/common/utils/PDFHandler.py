import PyPDF2


def readFilePdf(path):
    reader = PyPDF2.PdfReader(path)
    text = reader.pages[0].extract_text()
    print('PAGE =',len(reader.pages))
    return text
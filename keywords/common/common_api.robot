*** Settings ***
Resource        ${CURDIR}/../../resources/import.robot

*** Keywords ***

Create header of api url encoded
    [Documentation]    ${token} is Authorization
    [Arguments]    ${token}
    &{header}=    BuiltIn.Create dictionary    Content-Type=application/x-www-form-urlencoded    Authorization=Basic ${token}
    [Return]    &{header}

Create header of api
    [Documentation]    ${token} is Authorization
    [Arguments]    ${token}
    &{header}=    BuiltIn.Create dictionary    Content-Type=application/json    Authorization=Bearer ${token}
    [Return]    ${header}

Create header of api with email
    [Documentation]    ${token} is Authorization
    [Arguments]    ${token}    ${email}
    &{header}=    BuiltIn.Create dictionary    Content-Type=application/json    Authorization=Bearer ${token}    email=${email}
    [Return]    ${header}

Extract data
    [Arguments]    ${response_body}    ${jsonpath}
    [Documentation]    ${response_body} is response that you want to get value
    ...    ${jsonpath} is json path that want to get value from response
    ${data}=    JSONLibrary.Get value from json    ${response_body}    ${jsonpath}
    [Return]    ${data}

Create request body for get token
    [Arguments]    ${code}
    [Documentation]    ${code} came form login to website and get token call back from server
    ...    can able receieve token from keyword : oauth_feature.Open browser and login to get authorization code
    ${request_body}=    BuiltIn.Create dictionary    grant_type=${api_information['type']}    client_id=${api_information['client']}    code=${code}    redirect_uri=${api_information['website']}
    ${request_body}=    BuiltIn.Evaluate  json.dumps(${request_body})  json
    [Return]    ${request_body}

Send post request with x-www-from-urlencoded with secert key
    [Arguments]    ${http_url}    ${endpoint}    ${request_body}    ${username}=${account_information['account_api']['username']}    ${password}=${account_information['account_api']['password']}    ${expected_status}=200
    ${username_password}=    BuiltIn.Convert to bytes    ${username}:${password}
    ${encode_user_password}=    BuiltIn.Evaluate    base64.b64encode($username_password)    base64
    &{header}=    common_api.Create header of api url encoded    ${encode_user_password}
    ${response}=    REST.POST    endpoint=${http_url}${endpoint}    headers=&{header}    data=${request_body}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Send post request x-www-from
    [Arguments]    ${token}    ${http_url}    ${endpoint}    ${request_body}    ${expected_status}=200
    &{header}=    common_api.Create header of api url encoded    ${token}
    ${response}=    REST.POST    endpoint=${http_url}${endpoint}    headers=&{header}    data=${request_body}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Send post request x-www-from with header
    [Arguments]    ${http_url}    ${endpoint}    ${header}    ${request_body}    ${expected_status}=200
    ${response}=    REST.POST    endpoint=${http_url}${endpoint}    headers=&{header}    body=${request_body}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Send post request x-www-from-data with header
    [Arguments]    ${http_url}    ${endpoint}    ${header}    ${request_body}    ${expected_status}=200
    ${response}=    REST.POST    endpoint=${http_url}${endpoint}    headers=&{header}    data=${request_body}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Send put request x-www-from with header
    [Arguments]    ${http_url}    ${endpoint}    ${header}    ${request_body}    ${expected_status}=200
    ${response}=    REST.PUT    endpoint=${http_url}${endpoint}    headers=&{header}    body=${request_body}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Send get request x-www-from
    [Arguments]    ${token}    ${http_url}    ${endpoint}    ${expected_status}=200
    &{header}=    common_api.Create header of api url encoded    ${token}
    ${response}=    REST.GET    endpoint=${http_url}${endpoint}   headers=&{header}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Send get request x-www-from with parameter
    [Arguments]    ${token}    ${http_url}    ${endpoint}    ${parameter}    ${expected_status}=200
    &{header}=    common_api.Create header of api    ${token}
    ${response}=    REST.GET    endpoint=${http_url}${endpoint}   headers=&{header}    query=${parameter}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Send get request x-www-from with header email and parameter
    [Arguments]    ${token}    ${http_url}    ${endpoint}    ${email}     ${parameter}    ${expected_status}=200
    &{header}=    common_api.Create header of api with email    ${token}    ${email}    
    ${response}=    REST.GET    endpoint=${http_url}${endpoint}   headers=&{header}    query=${parameter}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Send get request
    [Arguments]    ${http_url}    ${endpoint}    ${header}    ${parameter}    ${expected_status}=200
    ${response}=    REST.GET    endpoint=${http_url}${endpoint}   headers=${header}    query=${parameter}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Send delete request x-www-from with header
    [Arguments]    ${http_url}    ${endpoint}    ${header}    ${request_body}    ${expected_status}=200
    ${response}=    REST.DELETE    endpoint=${http_url}${endpoint}    headers=&{header}    body=${request_body}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Extract response code and response message
    [Arguments]    ${response}
    ${response_code}=    common_api.Extract data    ${response}    $.responseCode
    ${response_message}=    common_api.Extract data    ${response}    $.responseMessage
    &{error_message}=    BuiltIn.Create dictionary
    Collections.Set to dictionary    ${error_message}    message=${response_message}[0]    code=${response_code}[0]
    [Return]    ${error_message}

Combine data
    [Arguments]    ${list_a}    ${list_b}    ${symbol}=-
    ${final_list}=    BuiltIn.Create list
    ${count_list}=    BuiltIn.Get length    ${list_a}
    FOR  ${i}  IN RANGE  ${count_list}
        ${combine}=    BuiltIn.Evaluate    "${list_a}[${i}]${symbol}${list_b}[${i}]"
        Collections.Append to list    ${final_list}    ${combine}
    END
    [Return]    ${final_list}

Convert date format in list
    [Arguments]    ${list_a}    ${result_format}=%d/%m/%Y
    ${date_list}=    BuiltIn.Create list
    FOR  ${i}  IN  @{list_a}
        ${convert_date}=    Datetime.Convert date    ${i}   result_format=${result_format}
        Collections.Append to list    ${date_list}    ${convert_date}
    END
    [Return]    ${date_list}

Convert currency
    [Arguments]    ${list_a}
    ${currency}=    BuiltIn.Create list
    FOR  ${i}  IN  @{list_a}
        ${round_decimal}    BuiltIn.Evaluate    "{:,}".format(${i})
        Collections.Append to list    ${currency}    ${round_decimal}
    END
    [Return]    ${currency}

Verify the value in the list is contain a two-digit id
    [Arguments]    ${list}    ${number}
    [Documentation]    ${list} is list that want to verify
    ...    ${number} is number of regexp in case start with 52 fill only 52
    FOR  ${i}  IN  @{list}
        ${item_id}=    BuiltIn.Set variable    ${i}
        BuiltIn.Should match regexp    ${item_id}    ^${number}    msg=${msg_error['data_not_contain']}
    END

Verify the value in the list is contain value
    [Arguments]    ${list}    ${pattern}
    [Documentation]    ${list} is list that want to verify
    ...    ${pattern} is pattern of regexp e.g. ^20160621 it mean start with 20160621
    FOR  ${i}  IN  @{list}
        ${item_id}=    BuiltIn.Set variable    ${i}
        BuiltIn.Should match regexp    ${item_id}    ${pattern}    msg=${msg_error['data_not_contain']}
    END

Verify response code and response message
    [Documentation]    ${response} is response from api
    ...    ${expected_code} is expected status code that return from api e.g. 0000
    ...    ${expected_message} is expected message that return from api e.g. success
    [Arguments]    ${response}    ${expected_code}    ${expected_message}
    ${code_and_message}=    common_api.Extract response code and response message    ${response}
    BuiltIn.Should be equal    ${code_and_message}[code]    ${expected_code}    msg=${msg_error['actual_expected_not_match']}
    BuiltIn.Should be equal    ${code_and_message}[message]    ${expected_message}    msg=${msg_error['actual_expected_not_match']}

Compare number of data between api and database
    [Documentation]    ${response_api} is response from api
    [Arguments]    ${response_api}    ${sql_statement}
    ${length_of_data}=    BuiltIn.Get length    ${response_api}
    ${length_of_data}=    BuiltIn.Convert to string    ${length_of_data}
    DatabaseLibrary.Row count is equal to x     ${sql_statement}    ${length_of_data}    msg=${msg_error['number_of_data_is_not_matches']}

Extract name and error message
    [Arguments]    ${response}
    ${name}=    common_api.Extract data    ${response}    $..name
    ${message}=    common_api.Extract data    ${response}    $..errorMessage
    &{error_message}=    BuiltIn.Create dictionary
    Collections.Set to dictionary    ${error_message}    name=${name}    message=${message}
    [Return]    ${error_message}

Verify response code and response message is response correctly
    [Arguments]    ${response_code}    ${response_message}
    [Documentation]    ${response} is response from api
    ...    /n    ${response_code} is response code e.g. 0000
    ...    /n    ${responseMessage} is response message e.g. success
    REST.String    $..responseCode    ${response_code}
    REST.String    $..responseMessage    ${response_message}

Extract data from response upload item
    [Documentation]    ${response_api} is response api from get order details
    [Arguments]    ${response_api}
    ${total_accepts}=    common_api.Extract data    ${response_api}    $..totalAccepts
    ${total_errors}=    common_api.Extract data    ${response_api}    $..totalErrors
    &{upload_data}=    BuiltIn.Create dictionary
    Collections.Set to dictionary    ${upload_data}    total_accepts=${total_accepts}[0]    total_errors=${total_errors}[0]
    [Return]    ${upload_data}

Send get request x-www-from with header email parameter and menu
    [Arguments]    ${token}    ${http_url}    ${endpoint}    ${email}    ${parameter}    ${menu}    ${expected_status}=200
    &{header}=    common_api.Create header of api with email and menu    ${token}    ${email}    ${menu}
    ${response}=    REST.GET    endpoint=${http_url}${endpoint}   headers=&{header}    query=${parameter}
    REST.Integer    response status    ${expected_status}
    [Return]    ${response}

Create header of api with email and menu
    [Documentation]    ${token} is Authorization
    [Arguments]    ${token}    ${email}    ${menu}
    &{header}=    BuiltIn.Create dictionary    Content-Type=application/json    Authorization=Bearer ${token}    email=${email}    menu=${menu}
    [Return]    ${header}

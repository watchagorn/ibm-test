*** Settings ***
Library        AppiumLibrary
Library        SeleniumLibrary
Library        ExcelLibrary
Library        OperatingSystem
Library        Pdf2TextLibrary
Library        Collections
Library        DateTime
Library        String
Library        ImapLibrary2
Library        BrowserMobProxyLibrary
Library        JSONLibrary
Library         REST



#common
Resource        ${CURDIR}/../keywords/common/common.robot
Resource        ${CURDIR}/../keywords/common/commonAppCommon.robot
#common
Resource        ${CURDIR}/../keywords/common/common_api.robot

#Utils
Library        ${CURDIR}/../keywords/common/utils/DateUtils.py
Library        ${CURDIR}/../keywords/common/utils/GeneralUtils.py
Library        ${CURDIR}/../keywords/common/utils/JsonUtils.py
Library        ${CURDIR}/../keywords/common/utils/ImageUtils.py
Library        ${CURDIR}/../keywords/common/utils/PDFHandler.py

#settings
Variables        ${CURDIR}/settings/settings.yaml
Variables        ${CURDIR}/settings/msg.yaml